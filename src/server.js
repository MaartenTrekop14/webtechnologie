import express from 'express';
import compression from 'compression';
import * as sapper from '@sapper/server';

import auctionRouter from './REST/auction';
import biddingRouter from './REST/bidding';
import userRouter from './REST/user';
import cors from "cors";

const {PORT, NODE_ENV} = process.env;
const dev = NODE_ENV === 'development';

express().use(cors)

express() // You can also use Express
    .use(
        compression({threshold: 0}),
        express.json(),
    )
    .use('/auction', auctionRouter)
    .use('/bidding', biddingRouter)
    .use('/user', userRouter)
    .use(sapper.middleware())
    .use(express.static('static'))
    .listen(PORT, err => {
        if (err) console.log('error', err);
    });

express().use(cors)
