import { writable } from 'svelte/store';

const tokenStore = writable();

export default tokenStore;