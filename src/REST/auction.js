const express = require('express');
const router = express.Router();
let items = require('./data/auctionitems');
const authenticateToken = require("../middleware/authentication");

router.get('', (req, res) => {
    // Create a copy of auction list
    let auctions = [...items];

    // Create filter attributes
    const {name, price, color, search} = req.query;

    if (search) {
        auctions = auctions.filter(item => item.name.toUpperCase().includes(search.toUpperCase()));
    }

    if (name) {
        auctions = auctions.filter((item) => item.name.includes(name));
        return res.status(200).json(auctions);
    }

    if (price) {
        // Check if price is a number
        if (Number.isNaN(parseInt(price))) {
            return res.status(404).json({error: 'Please enter a valid number'});
        }
        auctions = auctions.filter((item) => item.price <= parseInt(price));
        return res.status(200).json(auctions);
    }

    if (color) {
        auctions = auctions.filter((item) => item.color.includes(color));
        return res.status(200).json(auctions);
    }
    res.status(200).json(auctions);
})

router.get('/won', authenticateToken, (req, res) => {
    const dateNow = parseInt(new Date().toISOString().split('-').join(''));

    const wonAuctions = items.find((item) => {
        // Check if auction has ended
        const dateEnd = parseInt(item.endDate.split('-').join(''));
        if (dateNow > dateEnd) {

            // Check if last bidder is the user
            if (item.bids[item.bids.length - 1].name === req.user) {
                return item;
            }
        }
    });

    // Check if user has won any auctions
    if (wonAuctions.length === 0) {
        return res.status(404).json({message: 'You have won no auctions yet'});
    }
    res.status(200).json({won: wonAuctions});
});

router.post('', authenticateToken, (req, res) => {
    const {name, description, endDate, price, color, amount} = req.body;

    // Check if user has admin role
    if (req.user.role !== 'admin') {
        return res.status(403).json({message: 'User is not an admin.'});
    }

    const dateNow = new Date();
    const dateEnd = new Date(endDate);

    console.log(dateNow)
    // Check if string can be converted to a date object
    if (isNaN(new Date(endDate).getTime())) {
        return res.status(400).json({message: 'Incorrect date, please try again.'});
    }

    // Check if given date is in the past
    if (dateNow > dateEnd) {
        return res.status(400).json({message: 'Given date is in the past, please try again.'});
    }

    const auctionItem = {
        id: items.length + 1,
        name: name,
        description: description,
        endDate: new Date(endDate),
        price: price,
        amount: amount,
        color: req.body.color,
        bids: []
    }
    items.push(auctionItem);
    res.status(201).json({'Auction item added: ': auctionItem});
})

router.patch('/:id', authenticateToken, (req, res) => {
    const {name, price, color, amount, endDate, description} = req.body;


    // Check if user has admin role
    if (req.user.role !== 'admin') {
        return res.status(403).json({message: 'User is not an admin.'});
    }

    // Get item which will be modified
    const item = items.find((item) => item.id === parseInt(req.params.id));

    // return if no item has been found
    if (item === undefined) {
        return res.status(404).json({message: 'No item found.'});
    }

    // Check which attributes have to be modified and change them
    if (name) {
        item.name = name;
    }

    if (description) {
        item.description = description;
    }

    if (price) {
        item.price = price;
    }

    if (color) {
        item.color = color;
    }

    if (amount) {
        item.amount = amount;
    }


    if (endDate) {
        const dateNow = new Date().getTime();
        const dateEnd = new Date(req.body.endDate).getTime();

        // Check if string can be converted to a date object
        if (isNaN(new Date(endDate).getTime())) {
            return res.status(400).json({message: 'Incorrect date, please try again.'});
        }
        // Check if given date is in the past
        if (dateNow > endDate) {
            return res.status(400).json({message: 'Given date is in the past, please try again.'});
        }
        item.endDate = endDate;
    }


    res.status(201).json({'Auction item modified: ': item});
})

router.delete('/:id', authenticateToken, (req, res) => {
    const itemId = parseInt(req.params.id);

    // Check if user has admin role
    if (req.user.role !== 'admin') {
        return res.status(403).json({message: 'User is not an admin.'});
    }

    // Filter out item
    items = items.filter((item) => item.id !== itemId)

    res.status(201).json(items);
})

module.exports = router;