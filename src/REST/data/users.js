const {v4: uuidv4} = require('uuid');

module.exports = [

    {
        name: 'piet',
        email: 'piet@gmail.com',
        passwordHash: "$2b$10$.okkP7yj2zE0g2VjE9hsgeOWhElNDTlZLKWBblJMeAh24mDdkCYR6",
        role: 'admin'
    },

    {
        name: 'henk',
        email: 'henk@gmail.com',
        passwordHash: "$2b$10$s3jAcJ7V.bjzn46OmybdyO4.7quAczTSdSTytfhoilMztoN4FN12y",
        role: 'admin'
    },

    {
        name: 'peter',
        email: 'peter@gmail.com',
        passwordHash: "$2b$10$TNGRRwkWldE0iLI48I/uAOmpwMHcPZFiveoG3lr3ZxXb4hNcKOE42",
        role: 'user'
    },
]

