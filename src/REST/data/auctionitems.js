const items = [
    {
        'id': 1,
        'name': 'Ikea 365+',
        'description': "Verminder je hoeveelheid voedselafval door restjes te bewaren in een voorraaddoos met deksel en ze bij een andere maaltijd op te warmen.",
        'price': 4,
        'color': 'white',
        'amount': 4,
        "endDate": "12/22/2020 10:00",
        'time': 2,
        'bids': [
            {
                "auctionID": 1,
                "name": "Jan",
                "price": 1,
                "time": "15:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "Jaap",
                "price": 4,
                "time": "16:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "piet",
                "price": 5.69,
                "time": "16:34 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "Joep",
                "price": 7.40,
                "time": "17:00 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "Henk",
                "price": 7.75,
                "time": "18:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "Henrie",
                "price": 7.99,
                "time": "19:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "Mark rutte",
                "price": 8.02,
                "time": "20:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 1,
                "name": "piet",
                "price": 8.06,
                "time": "20:34 uur",
                "bestBid": "",
            }
        ]
    },

    {
        'id': 2,
        'name': 'Pruta',
        'description': "Handig voor het opbergen van beleg, het in porties bewaren van voedsel, e.d.\n" +
            "Doordat de pot doorzichtig is, kan je makkelijk vinden wat je zoekt.\n",
        'price': 0.99,
        'color': 'white',
        'amount': 4,
        'time': 4,
        "endDate": "12/22/2020 10:00",
        'bids': [
            {
                "auctionID": 2,
                "name": "Jan",
                "price": 1,
                "time": "10:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Jaap",
                "price": 1.10,
                "time": "11:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Joep",
                "price": 1.69,
                "time": "12:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Joep",
                "price": 2.40,
                "time": "13:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Henk",
                "price": 2.45,
                "time": "14:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Henrie",
                "price": 3.99,
                "time": "15:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Mark rutte",
                "price": 4.02,
                "time": "16:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 2,
                "name": "Karel",
                "price": 5,
                "time": "17:04 uur",
                "bestBid": "",
            }
        ]
    },

    {
        'id': 3,
        'name': 'Rinnig',
        'description': "Dit afdruiprek is dubbelzijdig, dus je kan ervoor kiezen het water direct in de spoelbak te laten lopen of het op te vangen en later weg te gooien.",
        'price': 2,
        'color': 'red',
        'amount': 4,
        'time': 7,
        "endDate": "12/22/2020 10:00",
        'bids': [
            {
                "auctionID": 3,
                "name": "Jan",
                "price": 1,
                "time": "10:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Jaap",
                "price": 2,
                "time": "11:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Joep",
                "price": 2.69,
                "time": "12:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Joep",
                "price": 3.40,
                "time": "13:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Henk",
                "price": 4.05,
                "time": "14:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Henrie",
                "price": 4.99,
                "time": "15:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "Mark rutte",
                "price": 5.02,
                "time": "16:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 3,
                "name": "piet",
                "price": 6,
                "time": "17:04 uur",
                "bestBid": "",
            }
        ]
    },

    {
        'id': 4,
        'name': 'Kvot',
        'description': "Kan ingeklapt worden om ruimte te besparen.",
        'price': 7,
        'rating': 3,
        'color': 'blue',
        'amount': 4,
        'time': 1,
        "endDate": "12/22/2020 10:00",
        'bids': [
            {
                "auctionID": 4,
                "name": "Jan",
                "price": 1,
                "time": "10:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Jaap",
                "price": 2,
                "time": "11:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Joep",
                "price": 2.69,
                "time": "12:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Joep",
                "price": 3.40,
                "time": "13:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Henk",
                "price": 4.05,
                "time": "14:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Henrie",
                "price": 4.99,
                "time": "15:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Mark rutte",
                "price": 5.02,
                "time": "16:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 4,
                "name": "Karel",
                "price": 6,
                "time": "17:04 uur",
                "bestBid": "",
            }
        ]
    },

    {
        'id': 5,
        'name': 'BESTÅENDE',
        'description': "Het afdruiprek kan worden vergroot door het blad uit te trekken, zodat je plaats krijgt voor meer vaat op een klein oppervlak.",
        'price': 19.95,
        'rating': 3,
        'color': 'yellow',
        'amount': 4,
        'time': 12,
        "endDate": "12/22/2030 10:00",
        'bids': [
            {
                "auctionID": 5,
                "name": "Jan",
                "price": 1,
                "time": "10:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Jaap",
                "price": 2,
                "time": "11:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Joep",
                "price": 2.69,
                "time": "13:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Joep",
                "price": 3.40,
                "time": "14:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Henk",
                "price": 4.05,
                "time": "15:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Henrie",
                "price": 4.99,
                "time": "16:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Mark rutte",
                "price": 5.02,
                "time": "17:04 uur",
                "bestBid": "",
            },
            {
                "auctionID": 5,
                "name": "Karel",
                "price": 6,
                "time": "18:04 uur",
                "bestBid": "",
            }
        ]
    },
]

module.exports = items;



