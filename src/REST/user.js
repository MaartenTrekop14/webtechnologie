const express = require('express');
const router = express.Router();
const users = require('./data/users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
require('dotenv').config();
const authenticateToken = require("../middleware/authentication");

router.get('', authenticateToken, (req, res) => {
    const admin = req.user.role === 'admin';

    if (!admin) {
        return res.status(401).send('You are not an admin');
    }
    return res.status(200).send(users);
})

router.post('/login', (req, res) => {
    const user = users.find((user) => user.email === req.body.email);

    if (!user) {
        return res.status(400).send({message: 'Email does not exist'});
    }

    const payload = {
        name: user.name,
        role: user.role
    }

    bcrypt.compare(req.body.password, user.passwordHash).then((result) => {
        if (result) {
            const authToken = jwt.sign(payload, process.env.TOKEN_SECRET);
            return res.json( authToken);
        } else {
            return res.status(400).json({message: "Invalid password"});
        }
    })
})

router.post('/register', async (req, res) => {
    const {name, email, password} = req.body;

    if (!name || !email || !password) {
        return res.status(400).send('Please fill in all of the fields');
    }

    const checkUser = users.find((user) => user.email === email);

    // Check if email already exists
    if (checkUser) {
        return res.status(400).send('Email already exists, please try a different email.');
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = {
        "name": name,
        "email": email,
        "passwordHash": hashedPassword,
        "role": 'user'
    };

    users.push(user);

    res.status(200).json({user: user});
})

module.exports = router;