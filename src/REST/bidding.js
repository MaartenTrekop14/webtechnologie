const items = require('./data/auctionitems');
const express = require('express');
const router = express.Router();
const authenticateToken = require("../middleware/authentication");

// GET all user bids
router.get('', authenticateToken, (req, res) => {
    let itemsWithUserBids = [];

    items.filter((item) => {
        for (let i = 0; i < item.bids.length; i++) {
            if (item.bids[i].name === req.user.name) {
                itemsWithUserBids.push(item);
                return;
            }
        }
    });
    res.status(201).json(itemsWithUserBids);
})

// POST a bid
router.post('/:id', authenticateToken, (req, res) => {
    // Parse itemId into a usable index
    const itemId = parseInt(req.params.id);

    // Get the item of item id
    const item = items.find((item) => item.id === itemId);

    // return if no item has been found
    if (item === undefined) {
        return res.status(404).json({message: 'No item found.'});
    }

    // Check if auction has ended
    // Create Date object of current date
    // Extract numeric values of date objects
    const dateNow = new Date().getTime();
    const dateEnd = new Date(item.endDate).getTime();

    if (dateNow > dateEnd) {
        return res.status(404).json({message: 'Auction has ended already.'});
    }

    const date = new Date();

    const bid = {
        'name': req.user.name,
        'price': req.body.price,
        "time": date.getHours() + ":" + date.getMinutes() + " uur"
    }

    // Check whether item has bids
    if (item.bids.length > 0) {

        // Return if bid is lower than, or equal to last bid
        if (item.bids[item.bids.length - 1].price >= bid.price) {
            return res.status(400).json({
                msg: 'Price has to be greater than the previous bid!'
            });
        }

        // Return if user of bid is also the last bidder
        if (item.bids[item.bids.length - 1].name === bid.name) {
            return res.status(400).json({
                msg: 'You are already the last bidder!'
            });
        }
    }
    item.bids.push(bid);
    res.status(201).json(item);
})

router.delete('/:id', authenticateToken, (req, res) => {
    // Parse itemId into a usable id
    const itemId = parseInt(req.params.id);

    // Get the item of item id
    const item = items.find((item) => item.id === itemId);

    // Passed all the checks, remove bid
    item.bids.pop();
    res.status(201).json(item.bids);
})

module.exports = router;