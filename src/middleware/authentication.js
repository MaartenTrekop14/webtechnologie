const jwt = require('jsonwebtoken');

function authenticateToken(req, res, next) {
    // Check if header has been set
    // Extract token from header

    const token = req.get("Authorization");

    if (!token) {
        return res.sendStatus(401).json({msg: 'Login has failed, please try again.'});
    }

    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) {
            console.log(err)
            return res.sendStatus(403).json({msg: 'Invalid token.'});
        }
        req.user = user;
        next();
    });
}

module.exports = authenticateToken;
