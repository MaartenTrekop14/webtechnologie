// JWT token string with header, payload and signature
import tokenStore from "../stores/token_store";
import {get} from "svelte/store";

let token;

tokenStore.subscribe( (data) => token = data)
export function saveToken(token) {
    localStorage.setItem('userToken', token);
}

export function resetToken() {
    tokenStore.update(oldValue => {
            return null;
        }
    )
}

export function getTokenPayload() {
    return JSON.parse(atob(token.split('.')[1]))
}