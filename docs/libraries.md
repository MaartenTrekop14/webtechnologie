# Wij gebruiken de onderstaande libraries:
1. Bootstrap
2. Express

## Bootstrap
Wij gebruiken Bootstrap om dat dat het maken van een website 100x makkelijker maakt.
Bootstrap is namelijk een mooie librarie om je website responsive te maken zonder veel moeite.
Verder bied Bootstrap ook veel classes voor tables enz. Dit maakt het overzichtelijk maken van een pagina met een tabel heel makkelijk

## Express
Express is het meest gebruikte framework voor het maken van Node.js applicaties. ook is ons in de lessen Express aangerande. <br>  
Andere opties waren:
1. Sanity.io
2. Polka
3. Nest.js
4. Koa.js
